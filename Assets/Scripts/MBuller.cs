﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MBuller : MonoBehaviour
{
    public int destroyTime = 2;
    void OnCollisionEnter(Collision cub)
    {
        if (cub.transform.tag == "Enemy"||cub.transform.tag=="Tack")
        {
            Destroy(this.gameObject);
        }
    }
    private void Update()
    {
        Destroy(this.gameObject, destroyTime);
    }
}
