﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    void OnCollisionEnter(Collision cub)
    {
        if (cub.transform.tag == "MBullet")
        {
            Destroy(this.gameObject);
        }
    }
}
