﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{
    public Text money;
    public Text timet;
    public TextMeshProUGUI endTime;
    public TextMeshProUGUI endMoney;
    public TextMeshProUGUI score;
    public static UIManager instance;
    public static float timer;
    public static bool timeStarted = false;
    public GameObject HUDPanel;
    public GameObject pausePanel;
    public GameObject winPanel;
    public Slider musicSlider;
    public Slider sfxSlider;


    void Awake()
    {
        instance = this;
    }
    public void SetItems(int items)
    {
        money.text = items.ToString();
        endMoney.text = money.text;
    }
    void Start()
    {
        timer = 0;
        timeStarted = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeStarted == true)
        {
            timer += Time.deltaTime;
        }
    }
    void CleanPanels()
    {
        HUDPanel.SetActive(false);
        pausePanel.SetActive(false);
    }
    public void ShowHUD()
    {
        CleanPanels();
        HUDPanel.SetActive(true);
        Time.timeScale = 1f;
        AnalyticsManager.instance.LogScreen("Play Screen");
    }
    public void ShowPause()
    {
        CleanPanels();
        pausePanel.SetActive(true);
        Time.timeScale = 0f;
        AnalyticsManager.instance.LogScreen("Pause Screen");
    }
    public void ShowWin()
    {
        CleanPanels();
        if (timer <= 140)
            score.text = "A";
        if (timer> 140&&timer<=160)
            score.text = "B";
        if (timer > 160&&timer<=180)
            score.text = "C";
        if (timer > 180)
            score.text = "D";
        endTime.text = timet.text;
        winPanel.SetActive(true);
        
    }
    public void SetMusicVolume(float volume)
    {
        musicSlider.value = volume;
    }

    public void GetMusicVolume(float sValue)
    {
        AudioManager.instance.SetMusicVolume(sValue);
    }

    public void SetSFXVolume(float volume)
    {
        sfxSlider.value = volume;
    }

    public void GetSFXVolume(float sValue)
    {
        AudioManager.instance.SetSFXVolume(sValue);
    }
    public void MainSceneLoader()
    {
        DataManager.instance.SaveData(GameManager.instance.player);
        AnalyticsManager.instance.LogScreen("Main Menu");
        SceneManager.LoadScene(0);
    }
    public void ExitGame()
    {
        DataManager.instance.SaveData(GameManager.instance.player);
        Application.Quit();
    }
    public void OnGUI()
    {
        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        string nTime = string.Format("{0:00}:{1:00}", minutes, seconds);

        timet.text = nTime;
    }
}
