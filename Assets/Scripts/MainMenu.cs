﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
    public GameObject optionsPanel;
    public GameObject instructPanel;
    public static MainMenu instance;


    public void StartGame()
    {
        Time.timeScale = 1f;
        
        SceneManager.LoadScene(1);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void Options()
    {
        optionsPanel.SetActive(true);
        //AnalyticsManager.instance.LogScreen("Options");
    }

    public void Return()
    {
        optionsPanel.SetActive(false);
        instructPanel.SetActive(false);
    }

    public void Instructions()
    {
        instructPanel.SetActive(true);
        //AnalyticsManager.instance.LogScreen("Instructions");
    }



}
