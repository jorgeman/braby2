﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int destroyTime = 2;
    void OnCollisionEnter(Collision cub)
    {
        if(cub.transform.tag == "Player")
            Destroy(this.gameObject);
    }
    private void Update()
    {
        Destroy(this.gameObject, destroyTime);
    }
}
