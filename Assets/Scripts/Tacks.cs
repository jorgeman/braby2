﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tacks : MonoBehaviour
{
    private BoxCollider c;

    void Start()
    {
        c = gameObject.GetComponent<BoxCollider>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player")
            c.enabled = false;
    }
}
