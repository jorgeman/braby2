﻿using System.IO;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public string playerFileName;
    public static DataManager instance;
    private Player player;
    private StreamReader sr;
    private StreamWriter sw;

    void Awake()
    {
        instance = this;
        player = new Player();
    }
    public Player GetData()
    {
#if FILE_SYSTEM
        if (File.Exists(Application.persistentDataPath+"/"+playerFileName))
        {
            sr = new StreamReader(Application.persistentDataPath + "/" + playerFileName);
            player=JsonUtility.FromJson<Player>(sr.ReadToEnd());
            Debug.Log("Load");
            sr.Close();
        }
#else
        if (PlayerPrefs.HasKey("Items"))
        {
            player.items = PlayerPrefs.GetInt("Items");
            player.musicVolume = PlayerPrefs.GetFloat("Music");
            player.sfxVolume = PlayerPrefs.GetFloat("SFX");
        }
#endif
        else
        {
            player.items = 0;
            player.musicVolume = 0;
            player.sfxVolume = 0;
        }
        return player;
    }

    public void SaveData(Player p)
    {
#if FILE_SYSTEM
        sw = new StreamWriter(Application.persistentDataPath+"/"+playerFileName, false);
        sw.Write(JsonUtility.ToJson(p));
        Debug.Log("Save");
        sw.Close();
#else
        PlayerPrefs.SetInt("Items", p.items);
        PlayerPrefs.SetFloat("Music", p.musicVolume);
        PlayerPrefs.SetFloat("SFX", p.sfxVolume);
        PlayerPrefs.Save();
        Debug.Log("Save");
#endif
    }
}