﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teddy : MonoBehaviour
{
    private Rigidbody teddyBody;
    private Vector3 pos;
    private Vector3 end;
    public int speed;
    void Start()
    {
        teddyBody = GetComponent<Rigidbody>();
        pos = transform.position;
        end.x = pos.x - 1.2f;
        end.z = pos.z + 4f;

    }

    // Update is called once per frame
    void Update()
    {
        teddyBody.transform.position = Vector3.Lerp(teddyBody.transform.position, end, speed * Time.deltaTime);
        teddyBody.transform.position = Vector3.Lerp(teddyBody.transform.position, pos, speed * Time.deltaTime);
    }
}
