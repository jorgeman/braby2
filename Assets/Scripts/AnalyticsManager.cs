﻿using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsManager : MonoBehaviour
{
    public GoogleAnalyticsV4 gAnalytics;
    public static AnalyticsManager instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        LogEvent("Main Event", "Start Session", "Init", System.DateTime.Now.Ticks);
        LogScreen("Play Screen");
    }

    public void LogEvent(string category, string action, string label, long value)
    {
        gAnalytics.LogEvent(category, action, label, value);
        Analytics.SendEvent(category, action);
    }

    public void LogScreen(string screenTitle)
    {
        gAnalytics.LogScreen(screenTitle);
        Analytics.CustomEvent(screenTitle);
    }
}
