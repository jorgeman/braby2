﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lasers : MonoBehaviour
{
    public GameObject bullet;
    public float force;
    private GameObject tmpBullet;
    void Start()
    {
        StartCoroutine(Shoot());
    }
    IEnumerator Shoot()
    {

        float time = 0;
        while (time < 5)
        {
            time += Time.deltaTime;
            tmpBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            tmpBullet.transform.forward = transform.up;
            tmpBullet.GetComponent<Rigidbody>().AddForce
                (transform.right * force, ForceMode.Impulse);
            yield return null;
        }
        while (time > 0)
        {
            time -= Time.deltaTime;
            yield return null;
        }
     

    }
}
