﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thug : MonoBehaviour
{
    public GameObject bullet;
    public float force;
    private GameObject tmpBullet;
    public float startTime;
    public float bulletTime;
    void Start()
    {
        InvokeRepeating("Shoot", startTime, bulletTime);
    }
    void Shoot()
    {
            tmpBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            tmpBullet.transform.forward = transform.up;
            tmpBullet.GetComponent<Rigidbody>().AddForce
                (transform.right * force, ForceMode.Impulse);
        
    }

}
