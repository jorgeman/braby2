﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Moshe : MonoBehaviour
{
    private NavMeshAgent agent;
    private Vector3 target;
    private Transform goal;
    public Transform start;
    public Transform chk1;
    public Transform chk2;
    private Animator anima;
    Animator Anim;
    public bool jump = false;
    public GameObject[] apples;
    CapsuleCollider capsule;
    public AudioSource cry;
    public AudioSource powerSound;
    public AudioSource respawn;
    public Material power;
    public Material velocity;
    public Material laser;
    public Material normal;
    public Material normaleyes;
    public GameObject shooter1;
    public SkinnedMeshRenderer romper;
    public SkinnedMeshRenderer eyes;
    private int i = 0;
    public float swipeMagnitude;
    private Touch fingerTouch;
    private Vector2 mosheMovement;
    private float deltaPos;
    private Vector2 startTouchPos;
    private float swipeAngle;
    private AnimatorStateInfo stateInfo;
    public float waitTime = 5f;
    public float jumpTime = 1f;
    public GameObject bullet;
    public float force;
    private GameObject tmpBullet;
    private bool check1=false;
    private bool check2=false;
    private bool apple = false;


    void Start()
    {
        //apples[3].SetActive(false);
        agent = GetComponent<NavMeshAgent>();
        anima = GetComponent<Animator>();
        goal = GameObject.FindGameObjectWithTag("Finish").transform;
        Anim = GetComponent<Animator>();
        cry = GetComponent<AudioSource>();
        capsule = GetComponent<CapsuleCollider>();
       // romper = GetComponent<SkinnedMeshRenderer>();
    }

    void Update()
    {

        target = goal.position;
        agent.SetDestination(target);
        /*    if (jump)
            {
                Anim.SetTrigger("Jump");
                jump = false;
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                Anim.SetTrigger("Slide");
            }*/
        stateInfo = Anim.GetCurrentAnimatorStateInfo(0);
        mosheMovement = Vector2.zero;
        //mosheMovement.y = dkRigidBody.velocity.y;
        for (int index = 0; index < Input.touchCount; index++)
        {
            fingerTouch = Input.GetTouch(index);
            switch (fingerTouch.phase)
            {
                case TouchPhase.Began:
                    startTouchPos = fingerTouch.position;
                    break;
                case TouchPhase.Ended:
                    deltaPos = (fingerTouch.position - startTouchPos).magnitude;
                    if (deltaPos >= swipeMagnitude && !stateInfo.IsName("Jump"))
                    {
                        swipeAngle = Vector2.Angle(Vector2.up, (fingerTouch.position - startTouchPos));
                        if (swipeAngle <= 45f)
                        {
                            Anim.SetTrigger("Jump");
                            StartCoroutine(Jumpy());
                            
                        }
                        
                    }

                    else
                    {
                        if (fingerTouch.position.x >= Screen.width * 0.5f)
                        {
                            Anim.SetTrigger("Slide");
                            StartCoroutine(Jumpy());
                        }


                    }
                    break;
            }
  

        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Tack"||collision.transform.tag=="Enemy")
        {
            cry.Play();
            if (i == 0)
            {
                if (apples[3].activeSelf)
                    apples[3].SetActive(false);
                else
                apples[i].SetActive(false);
            }
            if (i == 1)
            {
                if (apples[3].activeSelf)
                    apples[3].SetActive(false);
                else
                    apples[i].SetActive(false);
            }
            if (i == 2)
            {
                if (apple)
                {
                    apples[3].SetActive(false);
                    apple = false;
                    i = 1;
                }

                else
                    if (check1)
                {
                    Debug.Log("C1");
                    respawn.Play();
                    apples[i].SetActive(false);
                    transform.position = chk1.position;
                    apples[0].SetActive(true);
                    apples[1].SetActive(true);
                    apples[2].SetActive(true);
                    i = -1;
                }
                if (check2)
                {
                    Debug.Log("C2");
                    respawn.Play();
                    apples[i].SetActive(false);
                    transform.position = chk2.position;
                    apples[0].SetActive(true);
                    apples[1].SetActive(true);
                    apples[2].SetActive(true);
                    i = -1;
                }
                else
                {
                    Debug.Log("Inicio");
                    respawn.Play();
                    apples[i].SetActive(false);
                    transform.position = start.position;
                    apples[0].SetActive(true);
                    apples[1].SetActive(true);
                    apples[2].SetActive(true);
                    i = -1;
                }

                //SceneManager.LoadScene(1);

            }
            i++;


        }
        if (collision.transform.tag == "Test")
            Destroy(collision.transform.parent.gameObject);




    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Apple")
        {
            Destroy(other.gameObject);
            apples[3].SetActive(true);
            apple = true;
        }
        if (other.tag == "Money")
        {
            Destroy(other.gameObject);
            GameManager.instance.GetItem();
        }
        if (other.tag == "Checkpoint1")
        {
            check1 = true;
        }
        if (other.tag == "Checkpoint2")
        {
            check2 = true;
            check1 = false;
        }
        if (other.tag == "Win")
        {
            Anim.SetTrigger("Win");
            UIManager.instance.ShowWin();
            AnalyticsManager.instance.LogScreen("Win");
        }
        if (other.tag == "Laser")
        {
            powerSound.Play();
            Destroy(other.gameObject);
            romper.material = laser;
            eyes.material = laser;
            StopAllCoroutines();
            StartCoroutine(Fire());
            
        }
        if (other.tag == "Speed")
        {
            powerSound.Play();
            romper.material = velocity;
            Destroy(other.gameObject);
            StopAllCoroutines();
            StartCoroutine(Fast());
        }
        if (other.tag == "Power")
        {
            powerSound.Play();
            Destroy(other.gameObject);
            romper.material = power;
            StopAllCoroutines();
            StartCoroutine(Invincible());
        }
    }

    IEnumerator Jumpy()
    {
        float timej = 0;
        while (timej < jumpTime)
        {
            timej += Time.deltaTime;
            capsule.height = 0f;
            yield return null;
        }
        while (timej > 0)
        {
            timej -= Time.deltaTime;
            capsule.height = 6.2f;
            yield return null;
        }
    }

    IEnumerator Fast()
    {
        float timef = 0;
        while (timef < waitTime)
        {
            timef += Time.deltaTime;
            agent.speed = 20;
            yield return null;
        }
        while (timef > 0)
        {
            timef -= Time.deltaTime;
            agent.speed = 10;
            romper.material = normal;
            yield return null;
        }
    }
    IEnumerator Invincible()
    {
        float timei = 0;
        while (timei < waitTime)
        {
            timei += Time.deltaTime;
            capsule.height = 0;
            capsule.center = new Vector3(0, 6f, 0);
            yield return null;
        }
        while (timei > 0)
        {
            timei -= Time.deltaTime;
            capsule.height = 6f;
            capsule.center = new Vector3(0, 3.26f, 0);
            romper.material = normal;
            yield return null;
        }
    }
    IEnumerator Fire()
    {
        float timel = 0;
        while (timel < waitTime)
        {
            timel += Time.deltaTime;

            tmpBullet = Instantiate(bullet, shooter1.transform.position, shooter1.transform.rotation);
            tmpBullet.transform.forward = transform.up;
            tmpBullet.GetComponent<Rigidbody>().AddForce
                (transform.forward * force, ForceMode.Impulse);
            //shooter2.SetActive(true);
            yield return null;
        }
        while (timel > 0)
        {
            timel -= Time.deltaTime;

            //shooter2.SetActive(false);
            romper.material = normal;
            eyes.material = normaleyes;
            yield return null;
        }
    }
}
