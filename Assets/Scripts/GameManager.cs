﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public AudioSource earn;
    public Player player;

    void Awake()
    {
        instance = this;
        player = new Player();
    }
    void Start()
    {
        earn = GetComponent<AudioSource>();
    }
    public void GetItem()
    {
        earn.Play();
        player.items = player.items + 100;
        UIManager.instance.SetItems(player.items);
    }
    // Update is called once per frame
    void Update()
    {

    }
}